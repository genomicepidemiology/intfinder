# IntFinder
This is the repository for the IntFinder service. IntFinder is a freely available, online user-friendly tool for the detection of integrons in NGS data using kmer alignment (KMA).


## Documentation
The IntFinder service contains one python script intfinder.py which is the script of the latest version of the IntFinder service. 
The service identifies integrons in total or partial sequenced isolates of bacteria. 


## Content of the repository
- intfinder.py - the program.

- README.md - instructions.

- example_results - folder with example results.

- example.fasta - sequence for testing the program.


## Installation:
Setting up the IntFinder program
```bash
# Go to wanted location for intfinder
cd /path/to/some/dir
# Clone and enter the intfinder directory
git clone https://bitbucket.org/genomicepidemiology/intfinder.git
```
IntFinder is a python script. However to be able to run the program you need to install kma and download the accompaning database.

Download and install KMA and the IntFinder database
```bash
# Go to the directory where you want to store the intfinder database
cd intfinder
# Clone database from git repository (master branch)
git clone https://bitbucket.org/genomicepidemiology/kma.git
cd kma && make
cd ..
# Clone database from git repository (master branch)
git clone https://bitbucket.org/genomicepidemiology/intfinder_db.git
cd intfinder_db
INTFINDER_DB=$(pwd)
# Install intfinder database with executable kma_index program
python3 INSTALL.py
```

## Dependencies
In order to run the program Python 3.7 (or newer) should be installed along with the following versions of the modules (or newer).

#### Modules:
- cgecore 1.5.5

- tabulate 0.7.7

Modules can be installed using the following commands:
```bash
pip install cgecore
pip install tabulate
```
#### KMA
Additionally KMA version 2.8.1 or newer should be installed. The newest version of KMA can be installed from here:
```
https://bitbucket.org/genomicepidemiology/kma.git
```


## Usage:
The program can be invoked with the -h option to get help and more information of the service.

```bash
python3 intfinder.py -h

python3 intfinder.py --inputfile example.fasta --databasePath intfinderdb --methodPath kma/kma --outputPath example_results

python3 intfinder.py -i example.fasta -p intfinder_db -mp kma/kma -o example_results

python3 intfinder.py --inputfile exampleR1.fastq exampleR2.fastq
 --outputPath path/to/intfinder/results 
 --threshold 0.9
 --min_cov 0.9
 --minimum_depth 1.0 
  --databasePath path/to/intfinderdb
 --methodPath path/to/kma 
```
#### Recommendations:
Identity and coverage values set to 0.9 which corresponds to 90%. Depth is set to 0.9 (this is not a percentage).
A high threshold is recommended for discrimination.
During trials identical integrons to the ones in the database where found at >= 90% identity.

#### Output:
IntFinder produces 4 results files:

- results.txt

- results_tab.tsv

- Hit_in_genome_seq.fsa

- Integrons.fsa

The integrons are named as stated on INTEGRALL http://integrall.bio.ua.pt/.


## Web-server
A webserver implementing the methods is available at the CGE website and can be found here: https://cge.food.dtu.dk/services/IntFinder-1.0/


## When using the method please cite:
Torres-Elizalde, L., Ortega-Paredes, D., Loaiza, K., Fernández-Moreira, E., & Larrea-Álvarez, M. (2021). In Silico Detection of Antimicrobial Resistance Integrons in Salmonella enterica Isolates from Countries of the Andean Community. Antibiotics, 10(11), Article 11. https://doi.org/10.3390/antibiotics10111388


#### References:
- Torres-Elizalde, L., Ortega-Paredes, D., Loaiza, K., Fernández-Moreira, E., & Larrea-Álvarez, M. (2021). In Silico Detection of Antimicrobial Resistance Integrons in Salmonella enterica Isolates from Countries of the Andean Community. Antibiotics, 10(11), Article 11. https://doi.org/10.3390/antibiotics10111388

- Torres, L., Loaiza, K., Larrea-Álvarez, M., & Ortega-Paredes, D. (2021). Insights into the relationship between Salmonella enterica serovars and resistance integrons in the Andean Region using a novel bioinformatics tool: IntFinder. https://doi.org/10.13140/RG.2.2.25108.58241

- Torres, L., Loaiza, K., Larrea-Álvarez, M., & Ortega-Paredes, D. (2021). Relación entre serotipos de Salmonella enterica e integrones de resistencia en la Región Andina utilizando una novedosa herramienta bioinformática: IntFinder.

- Loaiza, K., Ortega-Paredes, D., Johansson, M., Florensa, A., Lund, O., & Bortolaia, V. (2020). IntFinder: A freely-available user-friendly web tool for detection of class 1 integrons in next-generation sequencing data using k-mer alignment. https://doi.org/10.13140/RG.2.2.17974.11844

- Clausen, P. T. L. C., Aarestrup, F. M., & Lund, O. (2018). Rapid and precise alignment of raw reads against redundant databases with KMA. BMC Bioinformatics, 19(1), 307. https://doi.org/10.1186/s12859-018-2336-6


## License
Copyright (c) 2020, Karen Loaiza, Technical University of Denmark All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
